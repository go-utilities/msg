package msg

import (
	"fmt"
	"time"

	"github.com/logrusorgru/aurora"
)

// PrintPlainln prints a plain text
func PrintPlainln(format string, a ...interface{}) {
	_, _ = fmt.Printf("    %s\n", aurora.Bold(fmt.Sprintf(format, a...)))
}

// PrintInfoln prints an info text
func PrintInfoln(format string, a ...interface{}) {
	_, _ = fmt.Printf("%s %s\n", aurora.Bold(aurora.BrightGreen("==>")), aurora.Bold(fmt.Sprintf(format, a...)))
}

// PrintMsgln prints a message
func PrintMsgln(format string, a ...interface{}) {
	_, _ = fmt.Printf("%s %s\n", aurora.Bold(aurora.BrightCyan("==>")), aurora.Bold(fmt.Sprintf(format, a...)))
}

// PrintWarnln prints a warning
func PrintWarnln(format string, a ...interface{}) {
	_, _ = fmt.Printf("%s %s\n", aurora.Bold(aurora.BrightYellow("==> WARNING:")), aurora.Bold(fmt.Sprintf(format, a...)))
}

// PrintErrorln prints an error
func PrintErrorln(format string, a ...interface{}) {
	_, _ = fmt.Printf("%s %s\n", aurora.Bold(aurora.BrightRed("==> ERROR:")), aurora.Bold(fmt.Sprintf(format, a...)))
}

// ProgressStr shows and moves a bar '...' on the command line. It can be used
// to show that an activity is ongoing. The parameter 'interval' steers the
// refresh rate (in milli seconds). The text in 'msg' is displayed in form of
// '...'. The progress bar is stopped by sending an empty struct to the
// returned channel:
//
//	chan <- struct{}{}
//	close(chan)
func ProgressStr(msg string, interval time.Duration) (chan<- struct{}, <-chan struct{}) {
	// create channel to receive stop signal
	stop := make(chan struct{})

	// create channel to send stop confirmation
	confirm := make(chan struct{})

	go func() {
		var (
			ticker  = time.NewTicker(interval * time.Millisecond)
			bar     = "   ...  "
			i       = 5
			isFirst = true
			ticked  = false
		)

		for {
			select {
			case <-ticker.C:
				// at the very first tick, the output switches to the next row.
				// At all subsequent ticks, the output is printed into that
				// same row.
				if isFirst {
					fmt.Println()
					isFirst = false
				}
				// print message and progress indicator
				fmt.Printf("\r%s %s ", msg, bar[i:i+3])
				// increase progress indicator counter for next tick
				if i--; i < 0 {
					i = 5
				}
				// ticker has ticked: set flag accordingly
				ticked = true
			case <-stop:
				// stop ticker ...
				ticker.Stop()
				// if the ticker had displayed at least once, move to next row
				if ticked {
					fmt.Println()
				}
				// send stop confirmation
				confirm <- struct{}{}
				close(confirm)
				// and return
				return
			}
		}
	}()

	return stop, confirm
}

// UserOK print the message s followed by " (Y/n)?" on stdout and askes the
// user to press either Y (to continue) or n (to stop). Y is treated as
// default. I.e. if the user only presses return, that's interpreted as if
// he has pressed Y.
func UserOK(s string) bool {
	var input string

	for {
		fmt.Printf("\r%s (Y/n)? ", s)
		if _, err := fmt.Scanln(&input); err != nil {
			if err.Error() != "unexpected newline" {
				return false
			}
			input = "Y"
		}
		switch {
		case input == "Y":
			return true
		case input == "n":
			return false
		}
		fmt.Println()
	}
}
